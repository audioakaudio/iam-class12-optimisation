﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bids_flapping : MonoBehaviour
{
    public GameObject Player;
    public AK.Wwise.Event birds_flapping;
    public bool TriggerOnce = false;
    private bool hasTriggered = false;




    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (TriggerOnce && hasTriggered)
            {
                return;
            }
            birds_flapping.Post(gameObject);
            hasTriggered = true;
        }
    }

}

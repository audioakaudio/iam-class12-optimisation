﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collider_footsteps : MonoBehaviour
{

public AK.Wwise.Event Npc_footsteps;
    private string _surfaceType;
    public string SwitchGroup_surface = "surface_type";
    public string SwitchGroup_locomotion = "locomotioin";


private void OnTriggerEnter(Collider Col)
{

AkSoundEngine.SetSwitch(SwitchGroup_locomotion, "walk", gameObject);
if (Col.CompareTag("Stone"))
        {
AkSoundEngine.SetSwitch(SwitchGroup_surface, "stone", gameObject);
            Npc_footsteps.Post(gameObject);
        }

if (Col.CompareTag("Grass"))
        {
AkSoundEngine.SetSwitch(SwitchGroup_surface, "grass", gameObject);
            Npc_footsteps.Post(gameObject);
        }

if (Col.CompareTag("Dirt"))
        {
AkSoundEngine.SetSwitch(SwitchGroup_surface, "sand", gameObject);
            Npc_footsteps.Post(gameObject);
        }

}



}

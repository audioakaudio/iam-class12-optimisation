/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMBIENT_BATTLE_ATMOSPHERE = 2259437719U;
        static const AkUniqueID AMBIENT_BEES = 188539821U;
        static const AkUniqueID AMBIENT_BIRDS_FLAPPING = 1202494564U;
        static const AkUniqueID AMBIENT_BLACKSMITH = 3321086392U;
        static const AkUniqueID AMBIENT_CEMETERY_DRONE_ZONEENTER = 1971015872U;
        static const AkUniqueID AMBIENT_CEMETERY_DRONE_ZONEEXIT = 3769281834U;
        static const AkUniqueID AMBIENT_CEMETERY_GATE = 2039066348U;
        static const AkUniqueID AMBIENT_CEMETERY_PEOPLE = 113143352U;
        static const AkUniqueID AMBIENT_CEMETERY_WHIMPER = 1326232935U;
        static const AkUniqueID AMBIENT_CHURCH_BELL = 1807080421U;
        static const AkUniqueID AMBIENT_CICADAS = 517927658U;
        static const AkUniqueID AMBIENT_CITY_BIRDS = 1608296306U;
        static const AkUniqueID AMBIENT_CITY_CHIRP = 2606578026U;
        static const AkUniqueID AMBIENT_CITY_KNOCK = 2733407630U;
        static const AkUniqueID AMBIENT_CITY_METAL_SQUEAK = 2912696720U;
        static const AkUniqueID AMBIENT_CITY_TAVERN_CROWD = 4231293238U;
        static const AkUniqueID AMBIENT_GATE_CLOSE = 865471206U;
        static const AkUniqueID AMBIENT_GATE_OPEN = 1170949682U;
        static const AkUniqueID AMBIENT_TENTS = 1958350812U;
        static const AkUniqueID AMBIENT_TREES = 3483076573U;
        static const AkUniqueID AMBIENT_WIND_CITY_ZONEENTER = 746809383U;
        static const AkUniqueID AMBIENT_WIND_CITY_ZONEEXIT = 422239951U;
        static const AkUniqueID AMBIENT_WIND_VILLAGE = 4182898825U;
        static const AkUniqueID CHARACTER_DEFENCE = 621078859U;
        static const AkUniqueID CHARACTER_FIGHTING_ARMS = 1747213339U;
        static const AkUniqueID CHARACTER_FIGHTING_IMPACT_DAMAGE = 1416295724U;
        static const AkUniqueID CHARACTER_FIGHTING_LEGS = 2282083553U;
        static const AkUniqueID CHARACTER_FOOTSTEPS = 2775932802U;
        static const AkUniqueID CHARACTER_JUMPING = 3057570747U;
        static const AkUniqueID CHARACTER_LANDING = 4274694430U;
        static const AkUniqueID MUSIC_MAIN = 1615767906U;
        static const AkUniqueID NPC_ENEMY_FOOTSTEPS = 2127739043U;
        static const AkUniqueID NPC_ENEMY_SWOOSH = 2677858539U;
        static const AkUniqueID NPC_FEMALE_FOOTSTEPS = 1872611041U;
        static const AkUniqueID NPC_FEMALE_VO = 2028544097U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace STATE_CASTLE_HALL
        {
            static const AkUniqueID GROUP = 2665360279U;

            namespace STATE
            {
                static const AkUniqueID STATE_CASTLE_HALL_INSIDE = 1774154446U;
                static const AkUniqueID STATE_CASTLE_HALL_OUTSIDE = 3223442859U;
            } // namespace STATE
        } // namespace STATE_CASTLE_HALL

        namespace STATE_COMBAT
        {
            static const AkUniqueID GROUP = 4122948419U;

            namespace STATE
            {
                static const AkUniqueID STATE_COMBAT_EXPLORATION = 1676259689U;
                static const AkUniqueID STATE_COMBAT_FIGHT = 2770195108U;
            } // namespace STATE
        } // namespace STATE_COMBAT

        namespace STATE_MUSIC_LOCATION
        {
            static const AkUniqueID GROUP = 1932435702U;

            namespace STATE
            {
                static const AkUniqueID STATE_MUSIC_LOCATION_CITY = 1010556712U;
                static const AkUniqueID STATE_MUSIC_LOCATION_VILLAGE = 2569934763U;
            } // namespace STATE
        } // namespace STATE_MUSIC_LOCATION

    } // namespace STATES

    namespace SWITCHES
    {
        namespace LOCOMOTIOIN
        {
            static const AkUniqueID GROUP = 500167305U;

            namespace SWITCH
            {
                static const AkUniqueID RUN = 712161704U;
                static const AkUniqueID WALK = 2108779966U;
            } // namespace SWITCH
        } // namespace LOCOMOTIOIN

        namespace SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 4064446173U;

            namespace SWITCH
            {
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID SAND = 803837735U;
                static const AkUniqueID STONE = 1216965916U;
            } // namespace SWITCH
        } // namespace SURFACE_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID RTPC_EXT_CAMERA_HEIGHT = 566631840U;
        static const AkUniqueID RTPC_INT_AZIMUTH = 406513959U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID NPC = 662417162U;
        static const AkUniqueID VILLAGE_AMBIENCE = 975238210U;
        static const AkUniqueID WIND = 1537061107U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID REV_CITY1 = 3533372987U;
        static const AkUniqueID REV_SMALL_ROOM = 1551944282U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__

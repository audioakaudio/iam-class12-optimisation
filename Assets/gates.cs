﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gates : MonoBehaviour
{
    Animator animator;
    public AK.Wwise.Event OpenEvent;
    public AK.Wwise.Event CloseEvent;
    bool doorOpen;

    private void Start()
    {
        doorOpen = false;
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            doorOpen = true;
            OpenEvent.Post(gameObject);
            Doors("Open");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(doorOpen)
        {
            doorOpen = false;
            CloseEvent.Post(gameObject);
            Doors ("Close");
        }
    }

    void Doors(string direction)
    {
        animator.SetTrigger(direction);
    }

}

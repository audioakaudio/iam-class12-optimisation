﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class character_sounds : MonoBehaviour
{
    public AK.Wwise.Event wwHit;
    public AK.Wwise.Event wwDamage;

    public void damagehit()
    {
        wwHit.Post(gameObject);
        wwDamage.Post(gameObject);
    }

    public void recoilhit()
    {
        wwHit.Post(gameObject);
    }
}
